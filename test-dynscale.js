let TEST = {
	canvas: null,
	width: 1920,
	height: 1080,
	json: {},
	thing: {},
	clones: [],
	msg: {}
};

function LOAD(json) {
	let data = json;
	let name = data.name || "anonymous";
	TEST.json[name] = data;
}
if(!console) {
	console = {
		log: function() {},
		error: function() {}
	};
}
function combineCallbacks(cbList, resultsVary, cb) {
	let results = [];
	let res = [];
	let uniq = [];
	while(results.length < cbList.length) {
		results.push(null);
	}
	cbList.every(function(callback, idx) {
		return callback(function(val) {
			res.push(val);
			results[idx] = val;
			if(uniq.indexOf(val) < 0) {
				uniq.push(val);
			}
			if(res.length === cbList.length) {
				if(uniq.length === 1) {
					cb(uniq[0], results);
				} else if(uniq.length > 1) {
					cb(resultsVary, results);
				} else {
					cb(null, results);
				}
			}
		});
	});
}

function animateGoon(timestamp) {
	let prog = ((timestamp - 0) / 500) % 1;
	let warrior = TEST.thing.warrior;
	if(prog < 0.25) {
		warrior.$.legs._rotate = ((prog * 4) * 95) - 5;
		warrior.$.arm._rotate = (prog * 4) * 15;
		warrior.$.body._rotate = (prog * 4) * 5;
	} else if(prog < 0.5) {
		warrior.$.legs._rotate = 90;
		warrior.$.arm._rotate = 15;
		warrior.$.body._rotate = 5;
	} else if(prog < 0.75) {
		warrior.$.legs._rotate = (-(((prog - 0.75) * 4) * 95)) - 5;
		warrior.$.arm._rotate = -((prog - 0.75) * 4) * 15;
		warrior.$.body._rotate = -((prog - 0.75) * 4) * 5;
	} else {
		warrior.$.legs._rotate = -5;
		warrior.$.arm._rotate = 0;
		warrior.$.body._rotate = 0;
	}
}

function tick(scene, time) {
	animateGoon(time);
}

function start() {
	//console.log("start");
	TEST.scene = new penduinSCENE(TEST.canvas, TEST.width, TEST.height,
								  tick, 60);
	TEST.scene.setBG("#463");
	TEST.scene.showFPS(true);
	TEST.scene.setDynamicScale(true);

	TEST.msg.controls = new penduinTEXT("+/- to add/remove instances",
										50, "white", false, false, true);
	TEST.msg.controls.x = TEST.width / 2;
	TEST.msg.controls.y = TEST.height / 2;
	TEST.scene.addTEXT(TEST.msg.controls);

	TEST.scene.addOBJ(TEST.thing.warrior);
	TEST.thing.warrior.x = TEST.width / 2;
	TEST.thing.warrior.y = TEST.height * 2/3;

	TEST.clones = buildInstances(8 * 8);
	TEST.thing.warrior.setInstances(TEST.clones);
	/*
*/
}

function buildInstances(howmany) {
	let maxX = Math.ceil(Math.sqrt(howmany));
	let maxY = maxX
	let i = 0;
	let arr = [];
	for(let y = 0; y < maxY; ++y) {
		for(let x = 0; x < maxX; ++x) {
			if(++i > howmany) {
				break;
			}
			arr.push({
				x: x * (TEST.width / maxX),
				y: (y+1) * (TEST.height / maxY)
			});
		}
	}
	return arr;
}

window.addEventListener("keydown", function(e) {
	switch(e.code) {
	case "Minus":
	case "NumpadSubtract":
		TEST.clones = buildInstances(TEST.clones.length - 1);
		TEST.thing.warrior.setInstances(TEST.clones);
		break;
	case "Equal":
	case "Plus":
	case "NumpadAdd":
		TEST.clones = buildInstances(TEST.clones.length + 1);
		TEST.thing.warrior.setInstances(TEST.clones);
		break;
	default:
		break;
	}
	console.log(e.code + " " + TEST.clones.length); //173, 61
});

window.addEventListener("load", function() {
	TEST.canvas = document.querySelector("#display");
	let cbs = [];

	// load object armatures and backgrounds
	Object.keys(TEST.json).every(function(key) {
		cbs.push(function(cb) {
			TEST.thing[key] = new penduinOBJ(TEST.json[key], cb);
			return true;
		});
		return true;
	});

	combineCallbacks(cbs, null, start);
});
