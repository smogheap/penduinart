LOAD({
	"name": "warrior",
	"scale": 0.2,
	"above": [
		{
			"name": "body",
			"image": "image/body.png",
			"pivot": {
				"x": 364,
				"y": 750
			},
			"rotate": 359,
			"scale": 1,
			"alpha": 1,
			"offset": {
				"x": 130,
				"y": -570
			},
			"above": [],
			"below": [
				{
					"name": "arm",
					"image": "image/arm.png",
					"pivot": {
						"x": 589,
						"y": 705
					},
					"rotate": 354,
					"scale": 1,
					"alpha": 1,
					"offset": {
						"x": 225,
						"y": 605
					},
					"above": [],
					"below": []
				},
				{
					"name": "legs",
					"image": "image/legs.png",
					"pivot": {
						"x": 638,
						"y": 290
					},
					"rotate": 269,
					"scale": 1,
					"alpha": 1,
					"offset": {
						"x": 320,
						"y": 750
					},
					"above": [],
					"below": []
				}
			]
		}
	],
	"below": []
});
